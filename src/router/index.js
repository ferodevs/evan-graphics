import { createRouter, createWebHistory } from "vue-router";
import NewLayout from "../layout/NewLayout.vue";
import NewBlog from "../pages/NewBlog.vue";
import NewContact from "../pages/NewContact.vue";
import NewProjects from "../pages/NewProjects.vue";
import Blog_1 from "../pages/blogs/Blog_1.vue";
import Blog_2 from "../pages/blogs/Blog_2.vue";
import PageNotFound from "../pages/404.vue";
import NewHome from "../pages/NewHome.vue";
import FunButton from "../pages/FunButton.vue";
const routes = [
  {
    path: "/",
    component: NewLayout,
    meta: {
      title: "evan.graphics",
      metaTags: [
        {
          name: "description",
          content: "Artist, designer, developer",
        },
        {
          property: "og:description",
          content: "Artist, designer, developer",
        },
      ],
    },
    children: [
      {
        path: "",
        components: {
          default: NewHome,
        },
      },
      {
        path: "home",
        redirect: "/",
      },
      {
        path: "projects",
        components: {
          default: NewProjects,
        },
        meta: {
          title: "Projects | evan.graphics",
          metaTags: [
            {
              name: "description",
              content: "Work, projects, and hobbies.",
            },
            {
              property: "og:description",
              content: "Work, projects, and hobbies.",
            },
          ],
        },
      },
      {
        path: "contact",
        components: {
          default: NewContact,
        },
        meta: {
          title: "Contact | evan.graphics",
          metaTags: [
            {
              name: "description",
              content: "A list of ways to contact me.",
            },
            {
              property: "og:description",
              content: "A list of ways to contact me.",
            },
          ],
        },
      },
      {
        path: "fun-button",
        components: {
          default: FunButton,
        },
        meta: {
          title: "The Button | evan.graphics",
          metaTags: [
            {
              name: "description",
              content: "THE BUTTON!",
            },
            {
              property: "og:description",
              content: "THE BUTTON!",
            },
          ],
        },
      },
      {
        path: "/blog",
        component: NewBlog,
        meta: {
          title: "Blog | evan.graphics",
          metaTags: [
            {
              name: "description",
              content: "Technical writing, rants, and obsessions.",
            },
            {
              property: "og:description",
              content: "Technical writing, rants, and obsessions.",
            },
          ],
          transition: "sidebar-fade",
        },
        children: [
          {
            path: "1",
            components: {
              default: Blog_1,
            },
            meta: { hideNavigation: true },
          },

          {
            path: "2",
            components: {
              default: Blog_2,
            },
            meta: { hideNavigation: true },
          },
        ],
      },
    ],
  },
  { path: "/:pathMatch(.*)*", component: PageNotFound },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// ...

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.title);

  const nearestWithMeta = to.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  const previousNearestWithMeta = from.matched
    .slice()
    .reverse()
    .find((r) => r.meta && r.meta.metaTags);

  if (nearestWithTitle) {
    document.title = nearestWithTitle.meta.title;
  } else if (previousNearestWithMeta) {
    document.title = previousNearestWithMeta.meta.title;
  }

  Array.from(document.querySelectorAll("[data-vue-router-controlled]")).map(
    (el) => el.parentNode.removeChild(el)
  );
  if (!nearestWithMeta) return next();

  nearestWithMeta.meta.metaTags
    .map((tagDef) => {
      const tag = document.createElement("meta");

      Object.keys(tagDef).forEach((key) => {
        tag.setAttribute(key, tagDef[key]);
      });

      tag.setAttribute("data-vue-router-controlled", "");

      return tag;
    })
    .forEach((tag) => document.head.appendChild(tag));

  next();
});

// ...

export default router;
